<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 7.12.2018
 * Time: 17:25
 */

namespace App\StatisticHelpers;


use App\StatisticHelpers\Enum\StatisticEnums;

abstract class TableStatisticBase extends StatisticBase implements ITableStatistics
{


    public $type = StatisticEnums::_TABLE;



}