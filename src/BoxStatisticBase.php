<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 7.12.2018
 * Time: 17:25
 */

namespace App\StatisticHelpers;


use App\StatisticHelpers\Enum\StatisticEnums;

abstract class BoxStatisticBase extends StatisticBase implements IBoxStatistic
{


    public function url()
    {
        return "#";
    }

    public $type = StatisticEnums::_BOX;
}