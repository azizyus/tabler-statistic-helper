<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 7.12.2018
 * Time: 16:44
 */

namespace App\StatisticHelpers;


interface ITableStatistics
{


    public function queryResult();

    public function tableColumns();


    /**
     * @return mixed
     *
     * u need to make plaint output for table which is not inclues methods, an array will be fine
     */
    public function tableData() : array;


}