<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 7.12.2018
 * Time: 16:44
 */

namespace App\StatisticHelpers;


interface IBoxStatistic
{


    public function count();

    public function url();

}